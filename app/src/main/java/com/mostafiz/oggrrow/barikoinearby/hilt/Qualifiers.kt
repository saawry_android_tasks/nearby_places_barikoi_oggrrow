package com.mostafiz.oggrrow.barikoinearby.hilt

import javax.inject.Qualifier

class Qualifiers {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class BarikoiCloud

}