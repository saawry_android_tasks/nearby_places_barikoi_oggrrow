package com.mostafiz.oggrrow.barikoinearby.hilt

import android.app.Application
import android.content.SharedPreferences
import androidx.annotation.Keep
import dagger.hilt.android.HiltAndroidApp
@Keep
@HiltAndroidApp
class AppInstance :Application(){
    override fun onCreate() {
        super.onCreate()
    }
}