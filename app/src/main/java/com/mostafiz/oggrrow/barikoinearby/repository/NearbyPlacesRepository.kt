package com.mostafiz.oggrrow.barikoinearby.repository

import com.mostafiz.oggrrow.barikoinearby.remote.ApiServiceNearbyPlaces
import com.mostafiz.oggrrow.barikoinearby.utils.NetworkResult
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class NearbyPlacesRepository @Inject constructor(private val apiServiceNearbyPlaces: ApiServiceNearbyPlaces) {

    suspend fun getNearbyPlaces(
        API_KEY: String,
        distance: String,
        limit: String,
        longitude: String,
        latitude: String,
        ptype: String
    ) = flow {
        emit(NetworkResult.Loading(true))
        val response = apiServiceNearbyPlaces.getNearbyPlaces(
            API_KEY,
            distance,
            limit,
            longitude,
            latitude,
            ptype
        )
        emit(NetworkResult.Success(response))
    }.catch { e ->
        emit(NetworkResult.Failure(e.message ?: "Unknown Error"))
    }

}