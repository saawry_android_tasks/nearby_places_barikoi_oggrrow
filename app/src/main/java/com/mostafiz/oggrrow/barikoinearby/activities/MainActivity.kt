package com.mostafiz.oggrrow.barikoinearby.activities

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.WellKnownTileServer
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mostafiz.oggrrow.barikoinearby.R
import com.mostafiz.oggrrow.barikoinearby.databinding.ActivityMainBinding
import com.mostafiz.oggrrow.barikoinearby.databinding.DialogAskPermissionBinding
import com.mostafiz.oggrrow.barikoinearby.models.BarikoiPlacesResponse
import com.mostafiz.oggrrow.barikoinearby.utils.NetworkResult
import com.mostafiz.oggrrow.barikoinearby.viewmodels.NearbyPlacesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var alertDialog: AlertDialog

    private lateinit var sharedPref: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    private val requestPermissionsLauncherLocation =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions(),
            this::onPermissionGranted
        )

    private var firstLaunch = 0
    private var locationAllowed: Boolean = true
    private val PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    private var locationRequest: LocationRequest? = null
    private var fusedLocationClient: FusedLocationProviderClient? = null


    private lateinit var BARIKOI_API_KEY: String
    private lateinit var mapView: MapView
    private lateinit var maplibreMap: MapboxMap
    private lateinit var styleUrl: String


    private var myLat = 0.0
    private var myLng = 0.0


    private val nearbyPlacesViewModel: NearbyPlacesViewModel by viewModels()


    private lateinit var sheetBehavior: BottomSheetBehavior<View>

    private var DISTANCE = "1"
    private var LIMIT = "10"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        BARIKOI_API_KEY = getString(R.string.barikoi_key)
        styleUrl = getString(R.string.barikoi_style_url) + BARIKOI_API_KEY

        Mapbox.getInstance(this, null, WellKnownTileServer.Mapbox)



        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.loadingBar.visibility = View.VISIBLE
        //init shared pref
        sharedPref = getSharedPreferences("Oggrow_sp", MODE_PRIVATE)
        editor = sharedPref.edit()


        //init location
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 2000L).build()


        // Init the MapView
        mapView = binding.mapView
        mapView.onCreate(savedInstanceState)

        permissionCheck()

        sheetBehavior = BottomSheetBehavior.from<View>(binding.bottomSheetLay.bottomSheetLayout)

        binding.bottomSheetLay.bottomSheetArrow.setOnClickListener { v ->
            if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
        }

        sheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {}
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                binding.bottomSheetLay.bottomSheetArrow.rotation = slideOffset * 180
            }
        })

        binding.bottomSheetLay.retrieveBtn.setOnClickListener {
            if (validValues()) {
                binding.loadingBar.visibility = View.VISIBLE
                getNearbyPlaces(
                    API_KEY = BARIKOI_API_KEY,
                    distance = DISTANCE,
                    limit = LIMIT,
                    longitude = myLng.toString(),
                    latitude = myLat.toString(),
                    ptype = "Bank"
                )
            } else {
                Toast.makeText(this, "Input Valid Values", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun validValues(): Boolean {
        val distance = binding.bottomSheetLay.distanceEt.text.toString()
        if (distance.isEmpty()) {
            binding.bottomSheetLay.distanceEt.error = "empty"
            return false
        } else if (distance == "0") {
            binding.bottomSheetLay.distanceEt.error = "min 1 km"
            return false
        } else if (distance.toInt() > 5) {
            binding.bottomSheetLay.distanceEt.error = "max 5 km"
            return false
        } else {
            DISTANCE = distance
            binding.bottomSheetLay.distanceEt.error = null
        }

        val limit = binding.bottomSheetLay.limitEt.text.toString()
        if (limit.isEmpty()) {
            binding.bottomSheetLay.limitEt.error = "empty"
            return false
        } else if (limit == "0") {
            binding.bottomSheetLay.limitEt.error = "min 1 km"
            return false
        } else if (limit.toInt() > 50) {
            binding.bottomSheetLay.limitEt.error = "max 50"
            return false
        } else {
            LIMIT = limit
            binding.bottomSheetLay.limitEt.error = null
        }
        return true
    }

    private fun askPermission() {
        val dialogBuilder = AlertDialog.Builder(this)
        val permissionDialogBinding: DialogAskPermissionBinding =
            DialogAskPermissionBinding.inflate(LayoutInflater.from(this), null, false)
        dialogBuilder.setView(permissionDialogBinding.root)


        permissionDialogBinding.btnCancel.setOnClickListener { alertDialog.dismiss() }


        permissionDialogBinding.btnOk.setOnClickListener {
            alertDialog.dismiss()
            requestPermissionsLauncherLocation.launch(PERMISSIONS)
        }

        alertDialog = dialogBuilder.create()
        alertDialog.show()
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

    }

    private fun permissionCheck() {
        if (hasPermissions(PERMISSIONS) == 0) {
            askPermission()
        } else {
            if (isGPSEnabled()) {
                Log.d("Location", "gps enabled pc")
                getCurrentLocation()
            } else {
                turnOnGPS()
            }
        }
    }

    private fun onPermissionGranted(grantStates: Map<String, Boolean>) {
        for ((key, value) in grantStates) {
            Log.d("permission", "$key - $value")
            if (!value) {
                locationAllowed = false
                break
            }
        }
        firstLaunch = 1
        editor.putInt("firstLaunch", firstLaunch)
        editor.apply()
        if (!locationAllowed) {
            checkRational(PERMISSIONS)
        } else {
            if (isGPSEnabled()) {
                Log.d("Location", "gps enabled: ")
                getCurrentLocation()
            } else {
                turnOnGPS()
            }
        }
    }

    private fun hasPermissions(permissions: Array<String>): Int {
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Log.d("Location", "no permission")
                return 0
            }
        }
        Log.d("Permission", "has permission")
        return 1
    }

    private fun checkRational(PERMISSIONS: Array<String>) {
        var f = true
        firstLaunch = sharedPref.getInt("firstLaunch", 0)
        if (firstLaunch == 1) {
            for (permission in PERMISSIONS) {
                if (shouldShowRequestPermissionRationale(permission)) {
                    f = false
                    break
                }
            }
        }
        if (f && firstLaunch == 1) {

            showInContextUI()
        }
    }

    private fun showInContextUI() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.location_permission_required))
            .setMessage(
                getString(R.string.location_permission_required) + "\n" + getString(
                    R.string.location_permission_disclosure
                )
            )
            .setNeutralButton(getString(R.string.don_t_allow)) { dialog, _ -> dialog.dismiss() }
            .setPositiveButton(getString(R.string.allow)) { _, _ -> toSettings() }
            .create().show()
    }

    private fun toSettings() {
        val intent = Intent()
        val uri: Uri = Uri.fromParts("package", this.packageName, null)
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).data = uri
        startActivity(intent)
    }

    private fun turnOnGPS() {
        Log.d("Location", "gps turn")
        val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest!!)
        builder.setAlwaysShow(true)
        val result: Task<LocationSettingsResponse> = LocationServices.getSettingsClient(
            applicationContext
        )
            .checkLocationSettings(builder.build())
        result.addOnCompleteListener { task ->
            try {
                val response: LocationSettingsResponse = task.getResult(ApiException::class.java)
                if (response.locationSettingsStates!!.isGpsUsable) {
                    Toast.makeText(
                        this,
                        getString(R.string.null_location),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                } else {
                    getCurrentLocation()
                }
            } catch (e: ApiException) {
                when (e.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        val resolvableApiException = e as ResolvableApiException
                        resolvableApiException.startResolutionForResult(this@MainActivity, 2)
                    } catch (ex: IntentSender.SendIntentException) {
                        ex.printStackTrace()
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Toast.makeText(
                        this,
                        getString(R.string.unavailable_location),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun isGPSEnabled(): Boolean {
        val locationManager: LocationManager?
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun getCurrentLocation() {


        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Toast.makeText(
                this@MainActivity,
                "Permission Denied",
                Toast.LENGTH_SHORT
            ).show()
            return
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)




        fusedLocationClient!!.getCurrentLocation(Priority.PRIORITY_HIGH_ACCURACY, null)
            .addOnSuccessListener { location ->
                if (location != null) {
                     myLat = location.latitude
                     myLng = location.longitude

                    Log.d(
                        "GetCurrentLocation",
                        "getCurrentLocation high priority: " + myLat + " " + myLng
                    )

                    //load map
                    loadMap()
                }
            }

    }

    private fun loadMap() {
        mapView.getMapAsync { map ->
            maplibreMap=map
            // Set the style after mapView was loaded
            map.setStyle(styleUrl) {
                map.uiSettings.setAttributionMargins(15, 0, 0, 15)

                addMarkersToMap(LatLng(myLat,myLng))

            }
        }
    }

    private fun getNearbyPlaces(
        API_KEY: String,
        distance: String,
        limit: String,
        longitude: String,
        latitude: String,
        ptype: String
    ) {
        nearbyPlacesViewModel.getNearbyPlaces(API_KEY, distance, limit, longitude, latitude, ptype)
        nearbyPlacesViewModel.nearbyPlacesResponse.observe(this) {
            when (it) {
                is NetworkResult.Loading -> {
                    // binding.progressbar.isVisible = it.isLoading
                }

                is NetworkResult.Failure -> {
                    Toast.makeText(this, it.errorMessage, Toast.LENGTH_SHORT).show()
                    // binding.progressbar.isVisible = false
                    Log.d("GetData", "PlacesList error: " + it.errorMessage)
                }

                is NetworkResult.Success -> {
                    Log.d("GetData", "PlacesListResult : " + it.data.toString())

                    addMarkersToMap(it.data)
                }

            }
        }
    }


    private fun addMarkersToMap(data: BarikoiPlacesResponse) {
        val bounds = mutableListOf<LatLng>()
        bounds.add(LatLng(myLat, myLng))
        // Get bitmaps for marker icon
        val infoIconDrawable = ResourcesCompat.getDrawable(
            this.resources,
            R.drawable.baseline_account_balance_24,
            null
        )!!
        val bitmap = infoIconDrawable.toBitmap()


        // Add symbol for each point feature
        for (place in data.places){

            val latLng = LatLng(place.latitude.toDouble(), place.longitude.toDouble())
            bounds.add(latLng)

            // Contents in InfoWindow of each marker
            val title = place.name
            val distance = place.distance_in_meters
            val address = place.Address
            val area = place.area
            val subType = place.subType


            val icon = IconFactory.getInstance(this)
                .fromBitmap(bitmap)

            // Use MarkerOptions and addMarker() to add a new marker in map
            val markerOptions = MarkerOptions()
                .position(latLng)
                .title(title)
                .snippet(subType+"\n"+area+"\n"+distance+"\n"+address)
                .icon(icon)

            maplibreMap.addMarker(markerOptions)
        }
        binding.loadingBar.visibility = View.GONE
        // Move camera to newly added annotations
        maplibreMap.getCameraForLatLngBounds(LatLngBounds.fromLatLngs(bounds))?.let {
            val newCameraPosition = CameraPosition.Builder()
                .target(it.target)
                .zoom(it.zoom - 0.5)
                .build()
            maplibreMap.cameraPosition = newCameraPosition
        }
    }
    private fun addMarkersToMap(latLng : LatLng) {

        val infoIconDrawable = ResourcesCompat.getDrawable(
            this.resources,
            R.drawable.baseline_account_circle_24,
            null
        )!!
        val bitmap = infoIconDrawable.toBitmap()


            val icon = IconFactory.getInstance(this)
                .fromBitmap(bitmap)

            val markerOptions = MarkerOptions()
                .position(latLng)
                .title("User Position")
                .snippet("This is the current position of user")
                .icon(icon)

            maplibreMap.addMarker(markerOptions)


        val newCameraPosition = CameraPosition.Builder()
            .target(latLng)
            .zoom(14.0)
            .build()
        binding.loadingBar.visibility = View.GONE
        maplibreMap.cameraPosition = newCameraPosition

    }



    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }
}