package com.mostafiz.oggrrow.barikoinearby.remote

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.mostafiz.oggrrow.barikoinearby.hilt.Qualifiers
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideOkHttp() : OkHttpClient {
        return OkHttpClient.Builder()
            .build()
    }
    @Provides
    @Named("baseUrl")
    fun provideBaseUrl(): String {
        return "https://barikoi.xyz/v2/api/"
    }

    @Provides
    @Qualifiers.BarikoiCloud
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient,@Named("baseUrl") baseUrl:String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(okHttpClient)
            .build()
    }

    @Provides
    fun provideApiServiceNearbyPlaces(@Qualifiers.BarikoiCloud retrofit: Retrofit): ApiServiceNearbyPlaces {
        return retrofit.create(ApiServiceNearbyPlaces::class.java)
    }
}