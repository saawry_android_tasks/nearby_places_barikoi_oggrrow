package com.mostafiz.oggrrow.barikoinearby.remote


import com.mostafiz.oggrrow.barikoinearby.models.BarikoiPlacesResponse
import retrofit2.http.*

interface ApiServiceNearbyPlaces {
    @GET("search/nearby/category/{API_KEY}/{distance}/{limit}")
    suspend fun getNearbyPlaces(
        @Path("API_KEY") API_KEY: String,
        @Path("distance") distance: String,
        @Path("limit") limit: String,
        @Query("longitude") longitude: String,
        @Query("latitude") latitude: String,
        @Query("ptype") ptype: String
    ): BarikoiPlacesResponse

}