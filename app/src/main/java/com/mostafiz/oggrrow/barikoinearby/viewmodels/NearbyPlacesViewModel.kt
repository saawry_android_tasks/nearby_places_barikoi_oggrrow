package com.mostafiz.oggrrow.barikoinearby.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mostafiz.oggrrow.barikoinearby.models.BarikoiPlacesResponse
import com.mostafiz.oggrrow.barikoinearby.repository.NearbyPlacesRepository
import com.mostafiz.oggrrow.barikoinearby.utils.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class NearbyPlacesViewModel @Inject constructor(private val repository: NearbyPlacesRepository) :
    ViewModel() {
    private var _nearbyPlacesResponse = MutableLiveData<NetworkResult<BarikoiPlacesResponse>>()
    val nearbyPlacesResponse: LiveData<NetworkResult<BarikoiPlacesResponse>> = _nearbyPlacesResponse


    fun getNearbyPlaces(
        API_KEY: String,
        distance: String,
        limit: String,
        longitude: String,
        latitude: String,
        ptype: String
    ) {
        viewModelScope.launch {
            repository.getNearbyPlaces(API_KEY, distance, limit, longitude, latitude, ptype)
                .collect {
                    _nearbyPlacesResponse.postValue(it)
                }
        }
    }

}