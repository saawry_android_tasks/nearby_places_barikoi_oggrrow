package com.mostafiz.oggrrow.barikoinearby.models

import com.google.gson.annotations.SerializedName

data class BarikoiPlacesResponse(val places: List<Place>)


data class Place (
    val id: Long,
    val name: String,
    val distance_in_meters: Double,
    val longitude: String,
    val latitude: String,
    val pType: String,
    val Address: String,
    val area: String,
    val city: String,
    val postCode: Long,
    val subType: String,
    val uCode: String,
    @SerializedName("ST_AsText(location)")
    val stAsTextLocation: String
)